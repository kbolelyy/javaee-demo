import cdi.BookListener;
import cdi.BookService;
import cdi.BookServiceServiceImpNumbOne;
import cdi.BookServiceServiceImpNumbTwo;
import cdi.decorator.BookNameDecorator;
import cdi.interceptor.logging.InterceptorLogging;
import cdi.interceptor.logging.LoggerProducer;
import messages.AsynchronousMessageQueue;
import messages.MessageListen;
import messages.MesssagesQuque;
import org.apache.openejb.jee.EjbJar;
import org.apache.openejb.jee.MessageDrivenBean;
import org.apache.openejb.jee.StatelessBean;
import org.apache.openejb.jee.jpa.unit.PersistenceUnit;
import org.apache.openejb.junit.ApplicationComposer;
import org.apache.openejb.testing.Classes;
import org.apache.openejb.testing.Configuration;
import org.apache.openejb.testing.Module;
import org.junit.Assert;
import org.junit.runner.RunWith;
import timeout.SomeTimerService;
import transactions.SomeTransactionalBean;
import transactions.TestEntity;

import javax.ejb.EJB;
import javax.ejb.Timer;
import javax.inject.Inject;
import javax.jms.JMSException;
import javax.naming.Context;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

@RunWith(ApplicationComposer.class)
public class Test {

    @Inject
    BookService bookService;

    @EJB
    SomeTimerService someTimerService;

    @EJB
    SomeTransactionalBean someTransactionalBean;

    @EJB
    MesssagesQuque messageQueue;

    @EJB
    AsynchronousMessageQueue asynchronousMessageQueue;


    @Module
    @Classes(cdi = true,
            value = {BookServiceServiceImpNumbOne.class,
                    BookServiceServiceImpNumbTwo.class,
                    BookService.class,
                    LoggerProducer.class,
                    BookListener.class,
                    MessageListen.class},
            cdiInterceptors = {InterceptorLogging.class},
            cdiDecorators = BookNameDecorator.class)
    public EjbJar beans() {
        EjbJar ejbJar = new EjbJar();
        ejbJar.addEnterpriseBean(new StatelessBean(SomeTimerService.class));
        ejbJar.addEnterpriseBean(new StatelessBean(SomeTransactionalBean.class));
        ejbJar.addEnterpriseBean(new StatelessBean(MesssagesQuque.class));
        ejbJar.addEnterpriseBean(new StatelessBean(AsynchronousMessageQueue.class));
        ejbJar.addEnterpriseBean(new MessageDrivenBean(MessageListen.class));

        return ejbJar;
    }

    @Module
    public PersistenceUnit persistence() {
        PersistenceUnit unit = new PersistenceUnit("opa");
        unit.setJtaDataSource("opa");
        unit.setNonJtaDataSource("opaUnmanaged");
        unit.getClazz().add(TestEntity.class.getName());
        unit.setProperty("openjpa.jdbc.SynchronizeMappings", "buildSchema(ForeignKeys=true)");
        return unit;
    }

    @Configuration
    public Properties config() {
        Properties properties = new Properties();
        properties.put(Context.INITIAL_CONTEXT_FACTORY, "org.apache.openejb.core.LocalInitialContextFactory");
        properties.put("log4j.rootLogger", "debug,C");
        properties.put("log4j.appender.C", "org.apache.log4j.ConsoleAppender");
        properties.put("opa", "new://Resource?type=DataSource");
        properties.put("opa.JdbcDriver", "org.hsqldb.jdbcDriver");
        properties.put("opa.JdbcUrl", "jdbc:hsqldb:mem:opa;create=true;");
        properties.put("opa.user", "sa");
        properties.put("opa.password", "");
        properties.put("opa.JtaManaged", "true");
        return properties;

    }

    @org.junit.Test
    public void testCreateBook() {
        bookService.createBook();
    }


    @org.junit.Test
    public void testValid() {
        ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
        Validator validator = vf.getValidator();
        Set<ConstraintViolation<Test>> validate = validator.validate(this);
        System.out.println(validate.size());
        Iterator<ConstraintViolation<Test>> iterator = validate.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next().getMessage());
        }
    }

    @org.junit.Test(timeout = 3000)
    public void timerTest() {
        Timer eventForTimer = someTimerService.createEventForTimer();
    }

    @org.junit.Test
    public void testTransaction() throws HeuristicRollbackException, HeuristicMixedException, NotSupportedException, RollbackException, SystemException {
        someTransactionalBean.smbd();
        TestEntity opanki = someTransactionalBean.getEntity("Opanki");
        System.out.println(opanki.getId());
        System.out.println(opanki.getAsd());
    }


    @org.junit.Test(timeout = 4000)
    public void testMQ() throws JMSException {
        messageQueue.sendMessage();
        Assert.assertEquals("Hello you!", messageQueue.recieveMessage());

    }

    @org.junit.Test
    public void testMQAsync() {
        asynchronousMessageQueue.produce();
        asynchronousMessageQueue.recive();
        while (true){}
    }


}
