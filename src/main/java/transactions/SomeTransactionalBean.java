package transactions;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateful;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import java.util.List;

@Stateful
@TransactionManagement(TransactionManagementType.BEAN)
public class SomeTransactionalBean {

    @Resource
    SessionContext context;

    @PersistenceContext(unitName = "opa")
    EntityManager manager;

    public void smbd() throws SystemException, NotSupportedException, HeuristicRollbackException, HeuristicMixedException, RollbackException {
        TestEntity testEntity = new TestEntity();
        testEntity.setAsd("Opanki");
        UserTransaction userTransaction = context.getUserTransaction();
        userTransaction.begin();
        manager.persist(testEntity);
        userTransaction.commit();

    }

    public TestEntity getEntity(String asde){
        Query query = manager.createQuery("SELECT a FROM TestEntity a where a.asd = :asd");
        query.setParameter("asd", asde);
        return (TestEntity) query.getResultList().get(0);
    }


}
