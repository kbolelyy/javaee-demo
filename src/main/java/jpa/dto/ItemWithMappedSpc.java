package jpa.dto;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class ItemWithMappedSpc extends MappedSpC {

    @Id
    @GeneratedValue
    Long id;

    String itemwithmappedspc1 = "itemwithmappedspc1" ;
    String itemwithmappedspc2 = "itemwithmappedspc2";

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemwithmappedspc1() {
        return itemwithmappedspc1;
    }

    public void setItemwithmappedspc1(String itemwithmappedspc1) {
        this.itemwithmappedspc1 = itemwithmappedspc1;
    }

    public String getItemwithmappedspc2() {
        return itemwithmappedspc2;
    }

    public void setItemwithmappedspc2(String itemwithmappedspc2) {
        this.itemwithmappedspc2 = itemwithmappedspc2;
    }

}
