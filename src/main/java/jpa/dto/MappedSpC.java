package jpa.dto;


import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class MappedSpC {

    String mappedspc1 = "mappedspc1";
    String mappedspc2 = "mappedspc2";

    public String getMappedspc1() {
        return mappedspc1;
    }

    public void setMappedspc1(String mappedspc1) {
        this.mappedspc1 = mappedspc1;
    }

    public String getMappedspc2() {
        return mappedspc2;
    }

    public void setMappedspc2(String mappedspc2) {
        this.mappedspc2 = mappedspc2;
    }

}
