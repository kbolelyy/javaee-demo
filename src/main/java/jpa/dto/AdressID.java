package jpa.dto;

import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class AdressID implements Serializable {

    private String one;



    public String getOne() {
        return one;
    }

    public void setOne(String one) {
        this.one = one;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdressID adressID = (AdressID) o;
        return Objects.equals(one, adressID.one);
    }

    @Override
    public int hashCode() {
        return Objects.hash(one);
    }
}
