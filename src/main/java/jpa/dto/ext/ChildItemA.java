package jpa.dto.ext;

import javax.persistence.Entity;

@Entity
public class ChildItemA extends Item {

    private String childA1 = "childA1";
    private String childA2  = "childA2";


    public String getChildA1() {
        return childA1;
    }

    public void setChildA1(String childA1) {
        this.childA1 = childA1;
    }

    public String getChildA2() {
        return childA2;
    }

    public void setChildA2(String childA2) {
        this.childA2 = childA2;
    }


}
