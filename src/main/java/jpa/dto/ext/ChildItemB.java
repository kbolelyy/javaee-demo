package jpa.dto.ext;

import javax.persistence.Entity;

@Entity
public class ChildItemB extends Item {


    private String childB1 = "childB1";
    private String childB2 = "childB1";

    public String getChildB1() {
        return childB1;
    }

    public void setChildB1(String childB1) {
        this.childB1 = childB1;
    }

    public String getChildB2() {
        return childB2;
    }

    public void setChildB2(String childB2) {
        this.childB2 = childB2;
    }


}
