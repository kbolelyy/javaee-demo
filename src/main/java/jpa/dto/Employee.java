package jpa.dto;


import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.MapKeyColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SecondaryTable;
import javax.persistence.SecondaryTables;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "employa")
@SecondaryTables({@SecondaryTable(name = "city"), @SecondaryTable(name = "country")})
public class Employee implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private Integer age;
    private String secondName;

    @Enumerated(EnumType.STRING)
    private CreditCardType cardType;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "Tag")
    private List<String> tags;

    @OneToMany
    @JoinTable(name = "employee_house_adress", joinColumns = @JoinColumn(name = "employee_fk"),
    inverseJoinColumns = @JoinColumn(name = "house_fk"))
    private List<HouseAdress> houseAdress;


    public List<HouseAdress> getHouseAdress() {
        return houseAdress;
    }

    public void setHouseAdress(List<HouseAdress> houseAdress) {
        this.houseAdress = houseAdress;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    @Column(table = "city")
    private String city;
    @Column(table = "country")
    private String country;

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getCity() {
        return city;
    }

    public CreditCardType getCardType() {
        return cardType;
    }

    public void setCardType(CreditCardType cardType) {
        this.cardType = cardType;
    }

    public void setCity(String city) {

        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Employee(String name, Integer age, CreditCardType type) {
        this.name = name;
        this.age = age;
        this.cardType = type;
    }

    public Employee() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
