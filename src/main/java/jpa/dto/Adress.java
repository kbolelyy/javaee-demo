package jpa.dto;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class Adress {

    @EmbeddedId
    private
    AdressID adressID;
    private String home;
    private String street;
    private String state;

    public String getHome() {
        return home;
    }

    public void setHome(String home) {
        this.home = home;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public AdressID getAdressID() {
        return adressID;
    }

    public void setAdressID(AdressID adressID) {
        this.adressID = adressID;
    }
}
