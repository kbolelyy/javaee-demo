package jpa.service;

import jpa.dto.Adress;
import jpa.dto.AdressID;
import jpa.dto.CreditCardType;
import jpa.dto.Employee;
import jpa.dto.HouseAdress;
import jpa.dto.ItemWithMappedSpc;
import jpa.dto.ext.ChildItemA;
import jpa.dto.ext.ChildItemB;
import jpa.dto.ext.Item;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;
import java.util.Arrays;

public class RunJpa {

    private static EntityManagerFactory emf =
            Persistence.createEntityManagerFactory("dto");


    public static void main(String[] args) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        Employee employee = new Employee("Johng", 18, CreditCardType.MASTER_CARD);
        employee.setCity("Some city1");
        employee.setCountry("Some country1");
        employee.setSecondName("secondName1");

        employee.setTags(Arrays.asList("asd", "ddwa", "yytre"));
        tx.begin();

        Adress adress = new Adress();
        adress.setHome("Home");
        adress.setStreet("Street");
        adress.setState("State");

        AdressID adressID = new AdressID();
        adressID.setOne("OneID");
        adress.setAdressID(adressID);

        HouseAdress houseAdress = new HouseAdress();
        houseAdress.setOwner("Koshkin dom");
        houseAdress.setEmployee(employee);
        employee.setHouseAdress(Arrays.asList(houseAdress));
        em.persist(employee);
        em.persist(houseAdress);
        em.persist(adress);


        Item item = new Item();
        ChildItemA childItemA = new ChildItemA();
        ChildItemB childItemB = new ChildItemB();
        em.persist(item);
        em.persist(childItemA);
        em.persist(childItemB);

        em.persist( new ItemWithMappedSpc());

        tx.commit();


        em.close();
        emf.close();


    }
}
