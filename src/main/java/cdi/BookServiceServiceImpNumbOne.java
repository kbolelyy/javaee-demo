package cdi;


import cdi.qualifer.BookOneQualifier;

@BookOneQualifier
public class BookServiceServiceImpNumbOne implements CreateBookService {

    @Override
    public Book createBook() {
        return  new Book("BookServiceServiceImpNumbOne", 200.32);
    }
}
