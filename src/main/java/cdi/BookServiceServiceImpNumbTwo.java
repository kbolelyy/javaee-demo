package cdi;


import cdi.qualifer.BookeTwoQualifier;

@BookeTwoQualifier
public class BookServiceServiceImpNumbTwo implements CreateBookService {

    @Override
    public Book createBook() {
        return new Book("BookServiceServiceImpNumbTWO", 123.24);
    }
}
