package cdi.interceptor.logging;


import cdi.qualifer.Loggable;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import java.text.MessageFormat;


@Interceptor
@Loggable
public class InterceptorLogging {

    @Inject
    Logger logger;

    @AroundInvoke
    public Object log(InvocationContext invocationContext) throws Exception {
        logger.debug(MessageFormat.format("entered to {0}.{1}()",
                invocationContext.getTarget().getClass().getName(),
                invocationContext.getMethod().getName()));
        try {
            return invocationContext.proceed();
        } finally {
            logger.debug(MessageFormat.format("exit from {0}.{1}()",
                    invocationContext.getTarget().getClass().getName(),
                    invocationContext.getMethod().getName()));

        }
    }
}
