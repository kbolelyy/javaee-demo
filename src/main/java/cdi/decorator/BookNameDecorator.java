package cdi.decorator;


import cdi.Book;
import cdi.CreateBookService;
import cdi.qualifer.BookOneQualifier;

import javax.decorator.Decorator;
import javax.decorator.Delegate;
import javax.inject.Inject;

@Decorator
public class BookNameDecorator implements CreateBookService {

    @Inject @BookOneQualifier @Delegate
    CreateBookService createBookService;

    @Override
    public Book createBook() {
        Book book = createBookService.createBook();
        book.setName(book.getName() + " with decorator");
        return book;
    }
}
