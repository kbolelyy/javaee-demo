package cdi;

import cdi.qualifer.BookOneQualifier;
import cdi.qualifer.BookeTwoQualifier;
import cdi.qualifer.Loggable;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.enterprise.event.Observes;
import javax.inject.Inject;


public class BookService {

    @Inject @BookOneQualifier
    CreateBookService createBookService;

    @Inject @BookeTwoQualifier
    CreateBookService createBookService2;

    @Inject
    Event<Book> bookEvent;


    @Loggable
    public void createBook(){
        Book book1 = createBookService.createBook();
        Book book = createBookService2.createBook();

        System.out.println(book1.getName() + " " + book1.getPrice());
        System.out.println(book.getName() + " " + book.getPrice());

        bookEvent.fire(book);
        bookEvent.fire(book1);
    }







}
