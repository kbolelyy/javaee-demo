package cdi;

import cdi.qualifer.Loggable;

import javax.enterprise.event.Observes;

public class BookListener {


    @Loggable
    public void listen(@Observes Book book){
        System.out.println(book.getName() + "from listener");
    }
}
