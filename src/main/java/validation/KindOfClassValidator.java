package validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

public class KindOfClassValidator implements ConstraintValidator<ClassAllowed, Object> {

    private List<String> typeOfClass = new ArrayList<>();

    @Override
    public void initialize(ClassAllowed classAllowed) {
        Class[] classes = classAllowed.classes();
        for (Class clazz : classes) {
            typeOfClass.add(clazz.getName());
        }
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        return typeOfClass.isEmpty() || typeOfClass.contains(o.getClass().getName());
    }
}
