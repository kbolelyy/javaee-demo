package timeout;


import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

@Stateless
public class SomeTimerService {

    @Resource
    private
    TimerService timerService;


    public Timer createEventForTimer() {
        Employee employee = new Employee();
        employee.setName("Mr who");
        employee.setSecondName("Family");
        Timer singleActionTimer = timerService.createSingleActionTimer(2000, new TimerConfig(employee, true));
       return singleActionTimer;

    }

    @Timeout
    public void sendMessageToConsole(Timer timer) {
        Employee info = (Employee) timer.getInfo();
        System.out.println("Ёптить вот и таймер для " + info.getName());

    }

}
