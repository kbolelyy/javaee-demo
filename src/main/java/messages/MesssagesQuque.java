package messages;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

@Stateless
public class MesssagesQuque {


    @Resource
    ConnectionFactory connectionFactory;

    @Resource
    Queue queue;


    public void sendMessage() {
        try (Connection connection = connectionFactory.createConnection()) {
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageProducer producer = session.createProducer(queue);
            TextMessage textMessage = session.createTextMessage("Hello you!");
            producer.send(textMessage);

        } catch (JMSException e) {
            e.printStackTrace();
        }

    }

    public String recieveMessage() throws JMSException {
        try (Connection connection = connectionFactory.createConnection()) {
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            MessageConsumer consumer = session.createConsumer(queue);
            connection.start();
            while (true){
                TextMessage receive = (TextMessage) consumer.receive();
                System.out.println("recive Message");
               return receive.getText();
            }
        }

    }


}
