package messages;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

@Stateless
@LocalBean
public class AsynchronousMessageQueue{


    @Resource
    ConnectionFactory connectionFactory;

    @Resource
    Queue queue;


    public void recive() {
        try (Connection connection = connectionFactory.createConnection()) {
            Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
            session.createConsumer(queue);
            connection.start();
        } catch (JMSException e) {
            e.printStackTrace();
        }

    }

    public void produce() {
        try (Connection connection = connectionFactory.createConnection()) {
            Session session = connection.createSession(true, Session.AUTO_ACKNOWLEDGE);
            MessageProducer producer = session.createProducer(queue);
            TextMessage it = session.createTextMessage("ЮХУ");
            producer.send(queue,it);
            connection.start();
        } catch (JMSException e) {
            e.printStackTrace();
        }


    }
}
